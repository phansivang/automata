from collections import defaultdict, deque
from itertools import chain
import networkx as nx
from pydot import Dot, Edge, Node
import automata.base.exceptions as exceptions
import automata.fa.fa as fa
from automata.base.utils import PartitionRefinement


class DFA(fa.FA):
    __slots__ = ('states', 'input_symbols', 'transitions',
                 'initial_state', 'final_states', 'allow_partial')

    def __init__(self, *, states, transitions,
                 initial_state, final_states, allow_partial=False):
        super().__init__(
            states=states,
            input_symbols={'a', 'b'},
            transitions=transitions,
            initial_state=initial_state,
            final_states=final_states,
            allow_partial=allow_partial
        )
        object.__setattr__(self, '_word_cache', [])
        object.__setattr__(self, '_count_cache', [])

    def __eq__(self, other):

        if not isinstance(other, DFA) or self.input_symbols != other.input_symbols:
            return NotImplemented

        operand_dfas = (self, other)
        initial_state_a = (self.initial_state, 0)
        initial_state_b = (other.initial_state, 1)

        def is_final_state(state_pair):
            state, operand_index = state_pair
            return state in operand_dfas[operand_index].final_states

        def transition(state_pair, symbol):
            state, operand_index = state_pair
            return (
                operand_dfas[operand_index]._get_next_current_state(
                    state, symbol),
                operand_index
            )

        state_sets = nx.utils.union_find.UnionFind((initial_state_a, initial_state_b))
        pair_stack = deque()

        state_sets.union(initial_state_a, initial_state_b)
        pair_stack.append((initial_state_a, initial_state_b))

        while pair_stack:
            q_a, q_b = pair_stack.pop()

            if is_final_state(q_a) ^ is_final_state(q_b):
                return False

            for symbol in self.input_symbols:

                r_1 = state_sets[transition(q_a, symbol)]
                r_2 = state_sets[transition(q_b, symbol)]

                if r_1 != r_2:
                    state_sets.union(r_1, r_2)
                    pair_stack.append((r_1, r_2))

        return True

    def __le__(self, other):
        if isinstance(other, DFA):
            return self.issubset(other)
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, DFA):
            return self.issuperset(other)
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, DFA):
            return self <= other and self != other
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, DFA):
            return self >= other and self != other
        else:
            return NotImplemented

    def __sub__(self, other):
        if isinstance(other, DFA):
            return self.difference(other)
        else:
            return NotImplemented

    def __or__(self, other):
        if isinstance(other, DFA):
            return self.union(other)
        else:
            return NotImplemented

    def __and__(self, other):
        if isinstance(other, DFA):
            return self.intersection(other)
        else:
            return NotImplemented

    def __xor__(self, other):
        if isinstance(other, DFA):
            return self.symmetric_difference(other)
        else:
            return NotImplemented

    def __invert__(self):
        return self.complement()

    def __iter__(self):
        i = self.minimum_word_length()
        limit = self.maximum_word_length()
        while limit is None or i <= limit:
            yield from self.words_of_length(i)
            i += 1

    def __len__(self):
        return self.cardinality()

    def _validate_transition_missing_symbols(self, start_state, paths):
        if self.allow_partial:
            return
        for input_symbol in self.input_symbols:
            if input_symbol not in paths:
                raise exceptions.MissingSymbolError(
                    'state {} is missing transitions for symbol {}'.format(
                        start_state, input_symbol))

    def _validate_transition_invalid_symbols(self, start_state, paths):
        for input_symbol in paths.keys():
            if input_symbol not in self.input_symbols:
                raise exceptions.InvalidSymbolError(
                    'state {} has invalid transition symbol {}'.format(
                        start_state, input_symbol))

    def _validate_transition_start_states(self):
        if self.allow_partial:
            return
        for state in self.states:
            if state not in self.transitions:
                raise exceptions.MissingStateError(
                    'transition start state {} is missing'.format(
                        state))

    def _validate_transition_end_states(self, start_state, paths):
        for end_state in paths.values():
            if end_state not in self.states:
                raise exceptions.InvalidStateError(
                    'end state {} for transition on {} is not valid'.format(
                        end_state, start_state))

    def _validate_transitions(self, start_state, paths):
        self._validate_transition_missing_symbols(start_state, paths)
        self._validate_transition_invalid_symbols(start_state, paths)
        self._validate_transition_end_states(start_state, paths)

    def validate(self):
        self._validate_transition_start_states()
        for start_state, paths in self.transitions.items():
            self._validate_transitions(start_state, paths)
        self._validate_initial_state()
        self._validate_final_states()
        return True

    def _get_next_current_state(self, current_state, input_symbol):
        if current_state is not None and input_symbol in self.transitions[current_state]:
            return self.transitions[current_state][input_symbol]
        return None

    def _check_for_input_rejection(self, current_state):
        if current_state not in self.final_states:
            raise exceptions.RejectionException(
                'the DFA stopped on a non-final state ({})'.format(
                    current_state))

    def read_input_stepwise(self, input_str, ignore_rejection=False):
        current_state = self.initial_state

        yield current_state
        for input_symbol in input_str:
            current_state = self._get_next_current_state(
                current_state, input_symbol)
            yield current_state

        if not ignore_rejection:
            self._check_for_input_rejection(current_state)

    def _get_digraph(self):
        return nx.DiGraph([
            (start_state, end_state)
            for start_state, transition in self.transitions.items()
            for end_state in transition.values()
        ])

    def _compute_reachable_states(self):
        visited_set = set()
        queue = deque()

        queue.append(self.initial_state)
        visited_set.add(self.initial_state)

        while queue:
            state = queue.popleft()

            for next_state in self.transitions[state].values():
                if next_state not in visited_set:
                    visited_set.add(next_state)
                    queue.append(next_state)

        return visited_set

    def minify(self, retain_names=False):
        reachable_states = self._compute_reachable_states()
        reachable_final_states = self.final_states & reachable_states

        return self._minify(
            reachable_states=reachable_states,
            input_symbols=self.input_symbols,
            transitions=self.transitions,
            initial_state=self.initial_state,
            reachable_final_states=reachable_final_states,
            retain_names=retain_names)

    @classmethod
    def _minify(cls, *, reachable_states, input_symbols, transitions, initial_state,
                reachable_final_states, retain_names):
        eq_classes = PartitionRefinement(reachable_states)
        refinement = eq_classes.refine(reachable_final_states)

        final_states_id = refinement[0][0] if refinement else next(iter(eq_classes.get_set_ids()))

        transition_back_map = {
            symbol: {
                end_state: list()
                for end_state in reachable_states
            }
            for symbol in input_symbols
        }

        for start_state, path in transitions.items():
            if start_state in reachable_states:
                for symbol, end_state in path.items():
                    transition_back_map[symbol][end_state].append(start_state)

        origin_dicts = tuple(transition_back_map.values())
        processing = {final_states_id}

        while processing:
            active_state = tuple(eq_classes.get_set_by_id(processing.pop()))
            for origin_dict in origin_dicts:
                states_that_move_into_active_state = chain.from_iterable(
                    origin_dict[end_state] for end_state in active_state
                )

                new_eq_class_pairs = eq_classes.refine(states_that_move_into_active_state)

                for (YintX_id, YdiffX_id) in new_eq_class_pairs:
                    if YdiffX_id in processing:
                        processing.add(YintX_id)
                    else:
                        if len(eq_classes.get_set_by_id(YintX_id)) <= len(eq_classes.get_set_by_id(YdiffX_id)):
                            processing.add(YintX_id)
                        else:
                            processing.add(YdiffX_id)

        eq_class_name_pairs = (
            [(frozenset(eq), eq) for eq in eq_classes.get_sets()] if retain_names else
            list(enumerate(eq_classes.get_sets()))
        )

        back_map = {
            state: name
            for name, eq in eq_class_name_pairs
            for state in eq
        }

        new_input_symbols = input_symbols
        new_states = frozenset(back_map.values())
        new_initial_state = back_map[initial_state]
        new_final_states = frozenset(back_map[acc] for acc in reachable_final_states)
        new_transitions = {
            name: {
                letter: back_map[transitions[next(iter(eq))][letter]]
                for letter in input_symbols
            }
            for name, eq in eq_class_name_pairs
        }

        return cls(
            states=new_states,
            input_symbols=new_input_symbols,
            transitions=new_transitions,
            initial_state=new_initial_state,
            final_states=new_final_states,
        )

    def union(self, other, *, retain_names=False, minify=True):

        def union_function(state_pair):
            q_a, q_b = state_pair
            return q_a in self.final_states or q_b in other.final_states

        new_states, new_transitions, new_initial_state, new_final_states = self._cross_product(
            other,
            union_function,
            should_construct_dfa=True,
            retain_names=retain_names
        )

        if minify:
            return self._minify(
                reachable_states=new_states,
                input_symbols=self.input_symbols,
                transitions=new_transitions,
                initial_state=new_initial_state,
                reachable_final_states=new_final_states,
                retain_names=retain_names)

        return self.__class__(
            states=new_states,
            input_symbols=self.input_symbols,
            transitions=new_transitions,
            initial_state=new_initial_state,
            final_states=new_final_states
        )

    def intersection(self, other, *, retain_names=False, minify=True):

        def intersection_function(state_pair):
            q_a, q_b = state_pair
            return q_a in self.final_states and q_b in other.final_states

        new_states, new_transitions, new_initial_state, new_final_states = self._cross_product(
            other,
            intersection_function,
            should_construct_dfa=True,
            retain_names=retain_names
        )

        if minify:
            return self._minify(
                reachable_states=new_states,
                input_symbols=self.input_symbols,
                transitions=new_transitions,
                initial_state=new_initial_state,
                reachable_final_states=new_final_states,
                retain_names=retain_names)

        return self.__class__(
            states=new_states,
            input_symbols=self.input_symbols,
            transitions=new_transitions,
            initial_state=new_initial_state,
            final_states=new_final_states
        )

    def _populate_count_cache_up_to_len(self, k):
        """
        Populate count cache up to length k
        """
        while len(self._count_cache) <= k:
            i = len(self._count_cache)
            self._count_cache.append(defaultdict(int))
            level = self._count_cache[i]
            if i == 0:
                level.update({state: 1 for state in self.final_states})
            else:
                prev_level = self._count_cache[i - 1]
                level.update({
                    state: sum(prev_level[suffix_state] for suffix_state in self.transitions[state].values())
                    for state in self.states
                })

    def show_diagram(self, path=None):

        graph = Dot(graph_type='digraph', rankdir='LR')
        nodes = {}
        for state in self.states:
            if state == self.initial_state:
                if state in self.final_states:
                    initial_state_node = Node(
                        'q' + state,
                        style='filled',
                        peripheries=2,
                        fillcolor='#66cc33')
                else:
                    initial_state_node = Node(
                        'q' + state, style='filled', fillcolor='#66cc33')
                nodes[state] = initial_state_node
                graph.add_node(initial_state_node)
            else:
                if state in self.final_states:
                    state_node = Node('q' + state, peripheries=2)
                else:
                    state_node = Node('q' + state)
                nodes[state] = state_node
                graph.add_node(state_node)
        # adding edges
        for from_state, lookup in self.transitions.items():
            for to_label, to_states in lookup.items():
                for to_state in to_states:
                    graph.add_edge(Edge(
                        nodes[from_state],
                        nodes[to_state],
                        label=to_label
                    ))
        if path:
            graph.write_png(path)
        return graph

    def dfa_to_nfa(self,dfa):
        nfa_transition = {state: {} for state in dfa['states']}
        for state, transitions in dfa['transition'].items():
            for symbol, new_state in transitions.items():
                if isinstance(new_state, set):
                    new_state = ','.join(sorted(new_state))
                nfa_transition[state][symbol] = new_state
            if '' in nfa_transition[state]:
                nfa_transition[state][''] = nfa_transition[state]['']
            else:
                nfa_transition[state][''] = state
        result = {
            'states': dfa['states'],
            'alphabet': dfa['alphabet'],
            'transition': nfa_transition,
            'initial_state': dfa['initial_state'],
            'final_states': dfa['final_states']
        }
        with open('transitions', 'w') as save:
            save.write(str(result['transition']))
            save.close()
