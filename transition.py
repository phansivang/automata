import customtkinter


class TransitionTable:
    def __init__(self, master, state):
        self.master = master
        self.tk = customtkinter
        self.state = state
        self.transitions = {}

    def column(self):

        a_column = self.tk.CTkLabel(master=self.master, text='A', font=("Arial", 70))
        a_column.place(x=750, y=20)

        b_column = self.tk.CTkLabel(master=self.master, text='B', font=("Arial", 70))
        b_column.place(x=950, y=20)

        e_column = self.tk.CTkLabel(master=self.master, text='EPS', font=("Arial", 70))
        e_column.place(x=1100, y=20)

        confirm = self.tk.CTkButton(master=self.master, text='CONFIRM', fg_color='green', command=self.close)
        final_state = self.tk.CTkOptionMenu(master=self.master, values=['q' + str(i) for i in range(0, self.state)],
                                            command=self.final_state)
        label = self.tk.CTkLabel(master=self.master, text='SELECT FINAL STATE')

        start_state = self.tk.CTkOptionMenu(master=self.master, values=['q' + str(i) for i in range(0, self.state)],
                                            command=self.start_state)
        label_state_stae = self.tk.CTkLabel(master=self.master, text='START STATE')

        for i in range(self.state):
            q_label = self.tk.CTkLabel(master=self.master, text=f'q{i}', font=("Arial", 40))
            q_label.place(x=620, y=110 + (i * 100))
            confirm.place(x=900, y=290 + (i * 100))
            start_state.place(x=900, y=200 + (i * 100))
            label_state_stae.place(x=750, y=200 + (i * 100))
            final_state.place(x=900, y=240 + (i * 100))
            label.place(x=750, y=240 + (i * 100))

    def button_value(self, i, j, value):
        symbol = 'a' if j == 0 else 'b' if j == 1 else ''
        numeric_value = int(value[1:])

        if i not in self.transitions:
            self.transitions[i] = {}

        if symbol not in self.transitions[i]:
            self.transitions[i][symbol] = set()

        self.transitions[i][symbol].add(str(numeric_value))

        nfa_transitions = {}
        for state, transitions in self.transitions.items():
            nfa_transitions[str(state)] = {}
            for symbol, next_states in transitions.items():
                nfa_transitions[str(state)][symbol] = ",".join(sorted(list(next_states)))

        with open('transitions', 'w') as f:
            f.write(str(nfa_transitions))

    def create_button(self, i, j, x, y):
        values = ['q{}'.format(n) for n in range(self.state)]
        transition_button = self.tk.CTkOptionMenu(
            master=self.master,
            values=values,
            font=('20', 20.0),
            command=lambda value: self.button_value(i, j, value)
        )
        transition_button.place(x=x, y=y)

    def create_buttons(self):
        for i in range(self.state):
            for j in range(3):
                x = 700 + j * 200
                y = 120 + i * 100
                self.create_button(i, j, x, y)

    def final_state(self, value):
        with open('final_state', 'w') as save:
            save.write(value[-1])
            save.close()

    def start_state(self, value):
        with open('state_state', 'w') as save:
            save.write(value[-1])
            save.close()

    def close(self):

        self.master.destroy()

# app = customtkinter.CTk()
# app.geometry('1080x1170')
# table = TransitionTable(master=app, state=5)
# table.column()
# table.create_buttons()
#
# app.mainloop()
