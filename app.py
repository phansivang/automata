import json
import customtkinter
from transition import TransitionTable
from nfa import NFA
from dfa import DFA
import ast
from tkinter import messagebox
from graph import Graph
import tkinter
from PIL import Image


class Application:
    def __init__(self, master):
        self.master = master
        self.tk = customtkinter
        self.tkinter = tkinter
        self.state = 0

    def set_state(self, state):
        data = self.state = int(state)
        return data

    def layout(self):
        self.tk.CTkLabel(master=app, text='Select State').place(x=50, y=20)
        self.tk.CTkLabel(master=app, text='Transitions').place(x=50, y=90)

        select_state = self.tk.CTkOptionMenu(master=app, values=[str(i) for i in range(1, 9)], command=self.set_state)
        select_state.pack(padx=10, pady=20)
        select_state.set('Select State')

        set_transitions = self.tk.CTkButton(self.master, command=self.transition, text='Set Transitions')
        set_transitions.pack(padx=10, pady=25)

        string_entry = self.tk.CTkEntry(self.master)
        string_entry.place(x=125, y=300)

        check_string = self.tk.CTkButton(self.master, command=lambda: self.check_data(string_entry.get()), text='Check '
                                                                                                                'String')
        check_string.place(x=200, y=350)
        self.tk.CTkButton(self.master, command=lambda: self.check_data(0), text='Validate').place(x=50, y=350)
        self.tk.CTkButton(self.master, command=lambda: self.check_data(2), text='Display Graph').place(x=50, y=400)
        self.tk.CTkOptionMenu(self.master, values=['DFA>NFA', 'NFA>DFA'], command=self.check_data).place(x=200, y=400)

    def transition(self):
        if self.state:
            screen = self.tk.CTkToplevel(self.tk.CTk())
            width = screen.winfo_screenwidth()
            height = screen.winfo_screenheight()
            screen.geometry(f'{width}x{height}')
            screen.resizable(False, False)
            screen.title('Transition Table')
            swidth = screen.winfo_screenwidth()
            height = screen.winfo_screenheight()
            x = (swidth // 2) - (width // 2)
            y = (height // 2) - (height // 2)
            screen.geometry('+{}+{}'.format(x, y))
            transition = TransitionTable(screen, self.state)
            transition.column()
            transition.create_buttons()
        else:
            messagebox.showerror('MISSING STATE', 'PLEASE SELECT STATE!')

    def graph(self):
        image_size = Image.open('transitions.png')
        width, height = image_size.size
        screen = self.tkinter.Toplevel()
        screen.resizable(False, False)
        screen.geometry(f'{width}x{height}')
        screen.iconbitmap('icon.ico')
        screen.title('Graph Transitions')
        width_screen = screen.winfo_screenwidth()
        height_screen = screen.winfo_screenheight()
        x = (width_screen // 2) - (width // 2)
        y = (height_screen // 2) - (height // 2)
        screen.geometry('+{}+{}'.format(x, y))
        display = Graph(screen)
        display.display()

    def check_data(self, value):
        [state, initial_state, final_state] = [set(map(str, range(self.state))), open('state_state', 'r').read(),
                                               open('final_state', 'r').read()]
        get_transitions = open('transitions', 'r').read()
        validate = self.validate_data(ast.literal_eval(get_transitions))
        transitions = json.loads(get_transitions.replace("'", "\""))
        try:
            dfa = DFA(states=state, transitions=validate, initial_state=initial_state, final_states={str(final_state)})
            self.data_condition(value, state, transitions, initial_state, {str(final_state)}, dfa)
            dfa.show_diagram('transitions.png')

        except:
            if value == 0: value = 1
            nfa = NFA(states=state, transitions=validate, initial_state=initial_state, final_states={str(final_state)})
            self.data_condition(value, state, transitions, initial_state, {str(final_state)}, nfa)
            nfa.show_diagram('transitions.png')

    def data_condition(self, value, state, transitions, initial_state, final_state, func):
        if value == 0:
            messagebox.showinfo('CHECK IS DFA OR NFA', 'THIS IS DFA')
        elif value == 1:
            messagebox.showinfo('CHECK IS DFA OR NFA', 'THIS IS NFA')
        elif value == 2:
            self.graph()
        elif value in ('DFA>NFA', 'NFA>DFA'):
            fa = {
                'states': state,
                'alphabet': {'a', 'b'},
                'transition': transitions,
                'initial_state': initial_state,
                'final_states': final_state
            }
            if value == 'DFA>NFA':
                func.dfa_to_nfa(fa)
            else:
                func.nfa_to_dfa(fa)
        else:
            self.check_string(func, value)

    @staticmethod
    def check_string(data, value):
        messagebox.showinfo('CHECK STRING', 'ACCEPTED !') if data.accepts_input(value) else messagebox.showerror(
            'CHECK STRING', 'REJECTED !')

    @staticmethod
    def validate_data(data):
        result = {}
        for key, value in data.items():
            if '' in value or ('a' in value and len(value['a'].split(',')) == 2) or (
                    'b' in value and len(value['b'].split(',')) == 2):
                new_value = {}
                if 'a' in value:
                    new_value['a'] = set(value['a'].split(','))
                if 'b' in value:
                    new_value['b'] = set(value['b'].split(','))
                if '' in value:
                    new_value[''] = set(value[''])
                result[key] = new_value
            else:
                result[key] = value
        return result


if __name__ == "__main__":
    app = customtkinter.CTk()
    title = app.title('Automaton')
    customtkinter.set_appearance_mode("dark")
    customtkinter.set_default_color_theme("blue")
    app.geometry(f'{400}x{500}')
    app.resizable(False, False)
    screen_width = app.winfo_screenwidth()
    screen_height = app.winfo_screenheight()
    x = (screen_width // 2) - (400 // 2)
    y = (screen_height // 2) - (500 // 2)
    app.geometry('+{}+{}'.format(x, y))
    window = Application(app)
    window.layout()

    app.mainloop()
