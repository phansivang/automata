from PIL import Image, ImageTk
import tkinter


class Graph:
    def __init__(self, master):
        self.master = master

    def display(self):
        load_image = Image.open("transitions.png")
        result = ImageTk.PhotoImage(load_image)
        display = tkinter.Label(master=self.master, image=result)
        display.image = result
        display.pack()
